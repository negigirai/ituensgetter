package swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import main.main;

public class Swing extends JFrame implements ActionListener{

	JFrame frame = new JFrame();
	JFileChooser fileChooser = new JFileChooser();
	JTextArea url,textArea;

	public Swing(){		
	}
	public void start(){
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		textArea = new JTextArea(5,20);
		JScrollPane jScrollPane = new JScrollPane(textArea);
		jScrollPane.setPreferredSize(new Dimension(100,100));
		panel.add(jScrollPane, BorderLayout.CENTER);
		JButton OKbutton = new JButton("起動");
		OKbutton.addActionListener(this);
		OKbutton.setActionCommand("起動");
		panel.add(OKbutton, BorderLayout.PAGE_END);

		JPanel URLPane = new JPanel();
		URLPane.setLayout(new BoxLayout(URLPane, BoxLayout.Y_AXIS));

		url = new JTextArea();
		JButton selectBtn = new JButton("フォルダ選択");
		selectBtn.addActionListener(this);
		selectBtn.setActionCommand("フォルダ選択");
		URLPane.add(url);
		URLPane.add(selectBtn);

		panel.add(URLPane, BorderLayout.PAGE_START);

		frame.add(panel);
		frame.setSize(300, 300);
		frame.setVisible(true);
	}

	public static void main(String[] arg){
		Swing s = new Swing();
		s.start();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String cmd = e.getActionCommand();

		if(cmd.equals("フォルダ選択")){
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int selectd = fileChooser.showSaveDialog(this);
			if(selectd == JFileChooser.APPROVE_OPTION){
				File file = fileChooser.getSelectedFile();
				url.setText(file.getAbsolutePath());
			}
		}else if(cmd.equals("起動")){
			if(!textArea.getText().equals("")){
				main main = new main();
				String[] getID = textArea.getText().split("\n");
				if(url.getText().equals("")){
					for(String ID : getID){
						String[] args = {ID};
						main.main(args);
					}	
				}else{
					for(String ID : getID){
						String[] args = {ID,url.getText()};
						main.main(args);
					}	
				}
			}			
		}else{
			JLabel label = new JLabel("IDを入力してください");
			label.setForeground(Color.RED);
			JOptionPane.showMessageDialog(this, label);
		}
	}
}



