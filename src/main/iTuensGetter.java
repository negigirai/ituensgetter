package main;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextArea;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class iTuensGetter{
	//private String charset = "Shift_JIS";
	private JTextArea htmlArea;
	private static final String UA = "iTunes/11.3.1";

	public iTuensGetter(URL url){
		htmlArea = new JTextArea();
		// Webページを読み込む
		try {
			// 接続
			URLConnection uc = url.openConnection();
			// HTMLを読み込む
			uc.setRequestProperty("User-Agent", UA);
			BufferedInputStream bis = new BufferedInputStream(uc.getInputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader(bis, "UTF-8"));
			htmlArea.setText("");//初期化
			String line;
			while ((line = br.readLine()) != null) {
				htmlArea.append(line + "\n\r");
			}
		} catch (MalformedURLException ex) {
			htmlArea.setText("URLが不正です。");
			ex.printStackTrace();
		} catch (UnknownHostException ex) {
			htmlArea.setText("サイトが見つかりません。");
		} catch (IOException ex) {
			ex.printStackTrace();
		}    
	}

	public void Show(){
		try {
			String html;
			html = htmlArea.getText();
			Pattern p = Pattern.compile("<script type=\"text/javascript\" charset=\"utf-8\">its.serverData=.*</script>");
			Matcher m = p.matcher(html);
			if(m.find()){
				JSONObject data;
				String description = "";

				data = new JSONObject(html.substring(m.start()+62, m.end()-9));
				if(data.getJSONObject("storePlatformData").getJSONObject("product-dv-product").getJSONObject("results")
						.has(main.ID)){
					description = data.getJSONObject("storePlatformData").getJSONObject("product-dv-product").getJSONObject("results")
						.getJSONObject(main.ID).getJSONObject("description").getString("standard");
				}
				JSONArray historyData = data.getJSONObject("pageData").getJSONObject("softwarePageData").getJSONArray("versionHistory");
				System.out.println("アプリ説明:");
				System.out.println(description);
				System.out.println("-----------------------------------------------------------\n\r-----------------------------------------------------------\n\r変更履歴:\n\r");

				for(int i = 0;i<historyData.length();i++){
					System.out.println("バージョン："+historyData.getJSONObject(i).getString("versionString"));
					System.out.println("更新日時："+historyData.getJSONObject(i).getString("releaseDate"));
					System.out.println("変更内容：\n\r"+historyData.getJSONObject(i).getString("releaseNotes") + "\n\r\n\r");
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void White() {

		try {
			String html = htmlArea.getText();
			Pattern p = Pattern.compile("<script type=\"text/javascript\" charset=\"utf-8\">its.serverData=.*</script>");
			Matcher m = p.matcher(html);
			if(m.find()){
				JSONObject data = new JSONObject(html.substring(m.start()+62, m.end()-9));
				String description = "";
				if(data.getJSONObject("storePlatformData").getJSONObject("product-dv-product").getJSONObject("results")
						.has(main.ID)){
					description = data.getJSONObject("storePlatformData").getJSONObject("product-dv-product").getJSONObject("results")
						.getJSONObject(main.ID).getJSONObject("description").getString("standard");
				}
				JSONArray historyData = data.getJSONObject("pageData").getJSONObject("softwarePageData").getJSONArray("versionHistory");
				File file = new File(main.URL + main.ID + ".txt");
				FileWriter fileWriter = new FileWriter(file);
				fileWriter.write("アプリ説明:\n\r");
				fileWriter.write(description);
				fileWriter.write("\n\r----------------------------------------------------------------------------------"
						+ "\n\r----------------------------------------------------------------------------------"
						+ "\n\r変更履歴:\n\r");

				for(int i = 0;i<historyData.length();i++){
					fileWriter.write("\n\rバージョン："+historyData.getJSONObject(i).getString("versionString"));
					fileWriter.write("\n\r更新日時："+historyData.getJSONObject(i).getString("releaseDate").substring(0, 10));
					fileWriter.write("\n\r変更内容：\n\r"+historyData.getJSONObject(i).getString("releaseNotes") + "\n\r\n\r");
				}
				fileWriter.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<historyData> getHistory(){
		ArrayList<historyData> history = new ArrayList<historyData>();
		White();
		String html = htmlArea.getText();//
		Pattern p = Pattern.compile("<script type=\"text/javascript\" charset=\"utf-8\">its.serverData=.*</script>");
		Matcher m = p.matcher(html);
		if(m.find()){

			try {
				JSONObject data = new JSONObject(html.substring(m.start()+62, m.end()-9));
				JSONArray historyData = data.getJSONObject("pageData").getJSONObject("softwarePageData").getJSONArray("versionHistory");
				for(int i=0;i<historyData.length();i++){
					historyData temp = new historyData();
					temp.setDescription(historyData.getJSONObject(i).getString("releaseNotes"));
					temp.setVersion(historyData.getJSONObject(i).getString("versionString"));
					temp.setDate(historyData.getJSONObject(i).getString("releaseDate").substring(0, 10));
					history.add(temp);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	

		return history;

	}


}
