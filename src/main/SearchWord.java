package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class SearchWord {

	private String BugfixesWords = "修正しました|修正しています|解消|を修正|問題を回避|調整";
	private String AddFuncWords = "をできるようにした|できるようになりました|を追加|使えるようにな|可能になりました";
	private String UpdateWords = "見直しました|を増やした|を変更しました|対応しました|に対応|にも対応|強化しました|可能になりました";
	private String UsabilityWords = "改善|最適化|わかりやすく";
	private String SecurityWords = "セキュリティ";
	private String InteroperabilityWords = "通信速度";
	private String SafetyWords = "安心";
	private String ScalabilityWords = "数+*増やす";
	private String AccuracyWords = "精度";
	private String PerformanceWords = "パフォーマンス";

	public SearchWord(){
		try {
			//ファイルを読み込む
			File fr;

			fr = new File("SearchWords.csv");

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fr),"Shift_JIS"));

			//読み込んだファイルを１行ずつ処理する
			String line;
			String[] token;
			while ((line = br.readLine()) != null) {
				//区切り文字","で分割する
				token = line.split(",");

				//分割した文字を格納する
				if(token[0].matches("BugfixesWords")){
					BugfixesWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							BugfixesWords += "|" + token[i];
					}
				} else if(token[0].matches("AddFuncWords")){
					AddFuncWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							AddFuncWords += "|" + token[i];
					}
				}else if(token[0].matches("UpdateWords")){
					UpdateWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							UpdateWords += "|" + token[i];
					}
				}else if(token[0].matches("UsabilityWords")){
					UsabilityWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							UsabilityWords += "|" + token[i];
					}
				}else if(token[0].matches("SecurityWords")){
					SecurityWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							SecurityWords += "|" + token[i];
					}
				}else if(token[0].matches("InteroperabilityWords")){
					InteroperabilityWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							InteroperabilityWords += "|" + token[i];
					}
				} else if(token[0].matches("SafetyWords")){
					SafetyWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							SafetyWords += "|" + token[i];
					}
				} else if(token[0].matches("ScalabilityWords")){
					ScalabilityWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							ScalabilityWords += "|" + token[i];
					}
				} else if(token[0].matches("AccuracyWords")){
					AccuracyWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							AccuracyWords += "|" + token[i];
					}
				} else if(token[0].matches("PerformanceWords")){
					PerformanceWords = token[1];
					if(token.length > 1){
						for(int i=2;i<token.length;i++)
							PerformanceWords += "|" + token[i];
					}
				}
			}

			//終了処理
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getBugfixesWords() {
		return BugfixesWords;
	}

	public String getAddFuncWords() {
		return AddFuncWords;
	}

	public String getUpdateWords() {
		return UpdateWords;
	}

	public String getUsabilityWords() {
		return UsabilityWords;
	}

	public String getSecurityWords() {
		return SecurityWords;
	}

	public String getInteroperabilityWords() {
		return InteroperabilityWords;
	}

	public String getSafetyWords() {
		return SafetyWords;
	}

	public String getScalabilityWords() {
		return ScalabilityWords;
	}

	public String getAccuracyWords() {
		return AccuracyWords;
	}

	public String getPerformanceWords() {
		return PerformanceWords;
	}

}
