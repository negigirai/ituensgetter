package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Result {
	private ArrayList<historyData> history;

	public Result(ArrayList<historyData> history){
		this.history = history;
		initWrite();
	}

	public void Parse(){
		for(int i = 0;i < history.size();i++){
			SumWords sw = new SumWords();
			SearchWord searchWord = new SearchWord();
			sw.setDate(history.get(i).getDate());
			sw.setVersion(history.get(i).getVersion());
			sw.setBugfixes(WordsCount(history.get(i).getDescription(),searchWord.getBugfixesWords()));
			sw.setAddFunc(WordsCount(history.get(i).getDescription(),searchWord.getAddFuncWords()));
			sw.setUpdate(WordsCount(history.get(i).getDescription(),searchWord.getUpdateWords()));
			sw.setUsability(WordsCount(history.get(i).getDescription(),searchWord.getUsabilityWords()));
			sw.setInteroperability(WordsCount(history.get(i).getDescription(),searchWord.getInteroperabilityWords()));
			sw.setSecurity(WordsCount(history.get(i).getDescription(),searchWord.getSecurityWords()));
			sw.setSafety(WordsCount(history.get(i).getDescription(),searchWord.getSafetyWords()));
			sw.setAccuracy(WordsCount(history.get(i).getDescription(),searchWord.getAccuracyWords()));
			sw.setPerformance(WordsCount(history.get(i).getDescription(),searchWord.getPerformanceWords()));
			Write(sw);
		}
	}

	private void initWrite(){
		try {
			File result = new File(main.URL + main.ID + ".csv"); // CSVデータファイル
			// 追記モード
			BufferedWriter bw;
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(result, false),"SJIS"));
			// 新たなデータ行の追加
			bw.write("日付,バージョン番号,不具合の修正,機能追加,修正,"
					+ "ユーザビリティ,セキュリティ,インタオペラビリティ,安心,精度,スケラビリティ,パフォーマンス");
			bw.newLine();
			bw.close();

		} catch (FileNotFoundException e) {
			// Fileオブジェクト生成時の例外捕捉
			e.printStackTrace();
		} catch (IOException e2) {
			// BufferedWriterオブジェクトのクローズ時の例外捕捉
			e2.printStackTrace();
		}
	}

	private void Write(SumWords sw){
		try {
			File result = new File(main.URL + main.ID + ".csv"); // CSVデータファイル
			BufferedWriter bw;
			//結果をSJISで書き込み
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(result, true),"SJIS"));
			// 新たなデータ行の追加
			bw.write(sw.getString());
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	private int WordsCount(String Text,String SearchWords){
		Pattern p = Pattern.compile(SearchWords);
		Matcher m = p.matcher(Text);
		int Count = 0;
		for(int start = 0;m.find(start);){
			Count++;
			start = m.start() + 2;
		}
		return Count;
	}
}
