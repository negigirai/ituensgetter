package main;

public class SumWords {
	private String date;
	private String version;
	private int bugfixes;
	private int update;
	private int addFunc;
	private int usability;
	private int security;
	private int interoperability;
	private int safety;
	private int scalability;
	private int accuracy;
	private int performance;
	public void setBugfixes(int bugfixes) {
		this.bugfixes = bugfixes;
	}
	public void setUpdate(int update) {
		this.update = update;
	}
	public void setAddFunc(int addFunc) {
		this.addFunc = addFunc;
	}
	public void setUsability(int usability) {
		this.usability = usability;
	}
	public void setSecurity(int security) {
		this.security = security;
	}
	public void setSafety(int safety) {
		this.safety = safety;
	}
	public void setScalability(int scalability) {
		this.scalability = scalability;
	}
	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	public String getString(){
		String result = date +","
				+ version + ","
				+ bugfixes + ","
				+ addFunc + ","
				+ update + ","
				+ usability + ","
				+ security + ","
				+ interoperability + ","
				+ safety + ","
				+ accuracy + ","
				+ scalability + ","
				+ performance;
		
		return result;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public void setInteroperability(int interoperability) {
		this.interoperability = interoperability;
	}
}
